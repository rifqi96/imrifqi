<?php
session_cache_limiter('nocache');
header('Expires: ' . gmdate('r', 0));

header('Content-type: application/json');

require 'php-mailer/class.phpmailer.php';

// Your email address
$to = 'rifqi96@yahoo.com';

$subject = $_POST['subject'];

if($to) {

    $name = $_POST['name'];
    $email = $_POST['email'];

    $fields = array(
        0 => array(
            'text' => 'Name',
            'val' => $_POST['name']
        ),
        1 => array(
            'text' => 'Email address',
            'val' => $_POST['email']
        ),
        2 => array(
            'text' => 'Message',
            'val' => $_POST['message']
        )
    );

    $message = "";

    foreach($fields as $field) {
        $message .= $field['text'].": " . htmlspecialchars($field['val'], ENT_QUOTES) . "<br>\n";
    }

    $mail = new PHPMailer(); // create a new object

    $mail->IsSMTP(); // enable SMTP

    // Optional Settings

    $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
    $mail->SMTPAuth = true; // authentication enabled
    $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
    $mail->Port = 465; // or 587
    $mail->IsHTML(true);

    $mail->Host = "smtp.gmail.com";
    $mail->Username = "rifqihit@gmail.com";
    $mail->Password = "";
    $mail->SMTPSecure = 'ssl';                          // Enable encryption, 'ssl' also accepted

    $mail->SetFrom($email,$name);
    $mail->AddAddress($to);								  // Add a recipient
    $mail->AddReplyTo($email, $name);

    $mail->IsHTML(true);                                  // Set email format to HTML

    $mail->CharSet = 'UTF-8';

    $mail->Subject = $subject;
    $mail->Body    = $message;

    if(!$mail->Send()) {
        $arrResult = array ('response'=>'error');
    }

    $arrResult = array ('response'=>'success');

    echo json_encode($arrResult);

} else {

    $arrResult = array ('response'=>'error');
    echo json_encode($arrResult);

}
?>